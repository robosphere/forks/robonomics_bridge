#!/bin/bash

RED="\033[1;31m"
YELLOW="\033[1;33m"
GREEN="\033[0;32m"
NO_COLOR="\033[0m"

SCRIPT_DIR="$(cd "$(dirname "${0}")"; pwd)"

fail() {
    echo -e "${RED}Command failed${NO_COLOR}"
    exit 1
}

