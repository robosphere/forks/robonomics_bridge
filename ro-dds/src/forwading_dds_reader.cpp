#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_DEBUG

#include <spdlog/spdlog.h>
#include <spdlog/common.h>

#include "rodds/dds_discovery.hpp"
#include "rodds/ro_dds.hpp"
#include "rodds/session_mgr.hpp"

/* TODO: 
 * implement dcps_reader with create_forwading_dds_reader 
 * for transfer data throw `SessionMgr`
*/

using namespace rodds::dds_discovery;

class Plugin
{
public:
    Plugin(
        const dds_entity_t &dp, boost::asio::local::stream_protocol::socket &rx, std::string &topic)
            : _reader(rx), _dp(dp), _topic(topic)
    {
        SPDLOG_INFO("Plugin initialized");
        read_message(1);
        SPDLOG_INFO("Go out from constructor");
    }

    void read_message(int id)
    {
        boost::asio::async_read_until(_reader, _buffer, " ",
            boost::bind(
                &Plugin::on_read_length, 
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred,
                id
                )                          
        ); 
    }

    void on_read_length(const boost::system::error_code &error, std::size_t bytes_trans, int id)
    {
        std::size_t length;
        char space;
        if (!error.failed() && 
            std::istream(&_buffer) >> std::noskipws >> length >> space && space == ' ')
        {
            if (length <= _buffer.size())
                on_read_message(error, 0, id);
            else
                boost::asio::async_read(_reader, _buffer,
                    boost::asio::transfer_exactly(length - _buffer.size()),
                    boost::bind(&Plugin::on_read_message, this,
                        boost::asio::placeholders::error,
                        boost::asio::placeholders::bytes_transferred,
                        id
                ));
        }
            
    }

    void on_read_message(const boost::system::error_code &error, std::size_t bytes_trans, int id)
    {
        std::istream msg(&_buffer);
        auto de = restore_from_wire<DiscoveryEvent>(msg);
        
        if (de.event_type == rodds::dds_discovery::DiscoveryEvent::DiscoveredPublication || 
            de.event_type == rodds::dds_discovery::DiscoveryEvent::DiscoveredSubscription)
        {
            SPDLOG_INFO(
                "Catch discovery event:{0}\n\ttopic_name: {1}\n\ttopic_type: {2}", 
                (de.event_type == 0? "DiscoveredPublication":
                 de.event_type == 1? "UndiscoveredPublication":
                 de.event_type == 2? "DiscoveredSubscription":
                 "UndiscoveredSubscription"),
                 de.entity->topic_name.c_str(), 
                 de.entity->topic_type.c_str());

            if (de.entity->topic_name == _topic && 
                de.event_type == rodds::dds_discovery::DiscoveryEvent::DiscoveredPublication)
            {
                dds_qos_t qos;
                std::chrono::system_clock::time_point period;
                _topic_reader = rodds::dds_discovery::create_forwading_dds_reader(
                    _dp, _topic, de.entity->topic_type, de.entity->key, qos, de.entity->keyless, 
                    period, NULL);
            }
        } else {
            SPDLOG_INFO("Catch discovery event:{0}",
                (de.event_type == 0? "DiscoveredPublication":
                 de.event_type == 1? "UndiscoveredPublication":
                 de.event_type == 2? "DiscoveredSubscription":
                 "UndiscoveredSubscription"));
        }

        std::cout << "its id: " << id << std::endl;
        if (!error)
            read_message(++id);
    }
 
private:
    dds_entity_t _topic_reader;
    std::string _topic;
    boost::asio::local::stream_protocol::socket& _reader;
    boost::asio::streambuf _buffer;
    dds_entity_t _dp;
};



int main(int argc, char* argv[])
{
    spdlog::set_level(spdlog::level::debug);
    // programm can create reader for only one dds_topic
    if (argc == 1 || argc > 2) 
    {
        SPDLOG_ERROR("Provide topic name for forwading reader process");
        return 0;
    }
    auto topic_name = std::string(argv[1]);
    SPDLOG_INFO("Provided topic to read: {0}", argv[1]);
    
    // create domain_participant, reader and writer 
    // sockets to catch rodds::dds_discovery::DiscoveryEvent`s
    SPDLOG_INFO("Generate DDS domain participant");
    //const dds_entity_t dp = dds_create_participant(0, NULL, NULL);
    boost::asio::io_service io_service;
    SPDLOG_INFO("Create reader/writer sockets");
    boost::asio::local::stream_protocol::socket tx(boost::asio::make_strand(io_service)), 
                                                rx(boost::asio::make_strand(io_service));
   
    boost::asio::local::connect_pair(tx, rx); 
    boost::asio::io_service::work work(io_service);

    rodds::RoDdsManager rm(io_service, tx, rx);
    
    rm.run();

    /*
    boost::asio::local::connect_pair(tx, rx); 

    boost::asio::io_service::work work(io_service);

    // create Plugin instance
    Plugin plugin(dp, rx, topic_name);

    rodds::dds_discovery::run_discovery(dp, &tx);
    
    io_service.run();
    */
    return 0;
}
