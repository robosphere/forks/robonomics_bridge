cmake_minimum_required(VERSION 3.12.0)

find_package(cppzmq REQUIRED)
find_package(Boost REQUIRED 
    COMPONENTS 
        thread 
        filesystem 
        serialization
        chrono
)
find_package(Threads REQUIRED)

add_library(ro-dds SHARED
    qos_util.cpp
    ros_discovery.cpp
    dds_discovery.cpp
    session_mgr.cpp
    ro_dds.cpp
)
target_include_directories(ro-dds 
    PUBLIC 
        ${PROJECT_SOURCE_DIR}/include
)

target_link_libraries(ro-dds 
    PUBLIC 
        cdds-util 
        cppzmq
        spdlog
        Boost::thread
        Boost::filesystem
        Boost::serialization
        Boost::chrono
)

# Headers
install(
    DIRECTORY ${PROJECT_SOURCE_DIR}/include/ 
    DESTINATION include
)

# Library
install (
    TARGETS 
        ro-dds
    LIBRARY DESTINATION 
        lib
)
