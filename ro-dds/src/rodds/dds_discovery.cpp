#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_DEBUG
#include <spdlog/spdlog.h>
#include <spdlog/fmt/fmt.h>

#include "rodds/dds_discovery.hpp"

namespace rodds::dds_discovery
{

static inline std::string get_hex_string(const uint8_t *begin, const uint8_t *end)
{
    std::stringstream ss;
    ss << std::hex;
    for(auto it = begin; it != end; ++it)
        ss << std::setw(2) << std::setfill('0') << int(*it);
    return ss.str();
}

void on_data(const dds_entity_t dr, void* arg)
{
    // cast socket where we what to send data about topic
    auto event_stream = 
        reinterpret_cast<std::pair<bool, boost::asio::local::stream_protocol::socket *> *>(arg);

    auto is_pub = event_stream->first;
    auto buf_tx = event_stream->second;
    dds_instance_handle_t dpih = 0;
    dds_sample_info_t sample_info[MAX_SAMPLES];
    void* samples[MAX_SAMPLES];
    samples[0] = NULL;

    int32_t num = dds_take(dr, samples, sample_info, (size_t)MAX_SAMPLES,(uint32_t)MAX_SAMPLES);

    for (int32_t i = 0; i < num; ++i)
    {
        auto sample = *(static_cast<dds_builtintopic_endpoint_t *>(samples[i]));
        bool is_alive = sample_info[i].instance_state == dds_instance_state::DDS_IST_ALIVE;

        auto key = get_hex_string(std::begin(sample.key.v), std::end(sample.key.v));
        auto participant_key = get_hex_string(std::begin(sample.participant_key.v),
            std::end(sample.participant_key.v));
        // only last one bit is indicate about keyless status
        bool keyless = *std::end(sample.key.v) == 3 || *std::end(sample.key.v) == 4;
        // TODO: check for invalid data!
        auto topic_name = std::string(sample.topic_name);
        auto topic_type = std::string(sample.type_name);
        auto qos = *sample.qos;

        // TODO: implement [TRACE/DEBUG] logging level 
        SPDLOG_DEBUG(
            "{0} DDS {1}\n\twith key {2}\n\tfrom Participant {3}\n\ton {4}\n\twith type {5}\n\tkeyless: {6}",
            (is_alive? "Discovered": "Undiscovered"),
            (is_pub? "publication": "subscription"),
            key,
            participant_key,
            topic_name,
            topic_type,
            keyless
        );
        
        if (topic_name.find("DCPS") != std::string::npos || 
            sample.participant_instance_handle == dpih)
        {    
            SPDLOG_DEBUG("Ignoring discovery of {0} ({1}) from local participant", 
                key, topic_name);
            continue;
        }

        DDSEntity entity = {
            .key = key
        };

        // Send discovery event
        if (is_alive)
        {
           entity = {
            .key = key,
            .participant_key = participant_key,
            .topic_name = topic_name,
            .topic_type = topic_type,
            .keyless = keyless,
            //.qos = qos
           };

           if (is_pub)
           {
                send_discovery_event(
                    dr, 
                    buf_tx, 
                    DiscoveryEvent{
                        std::make_shared<DDSEntity>(entity), 
                        DiscoveryEvent::DiscoveredPublication
                    }
                );
           }else{
                send_discovery_event(
                    dr, 
                    buf_tx,
                    DiscoveryEvent{
                        std::make_shared<DDSEntity>(entity),
                        DiscoveryEvent::DiscoveredSubscription
                    }
                );
           } 
        }else if (is_pub){
            send_discovery_event(
                dr, 
                buf_tx,
                DiscoveryEvent{
                    std::make_shared<DDSEntity>(entity), 
                    DiscoveryEvent::UndiscoveredPublication
                }
            ); 
        }else{
            send_discovery_event(
                dr,
                buf_tx,
                DiscoveryEvent{
                    std::make_shared<DDSEntity>(entity),
                    DiscoveryEvent::UndiscoveredSubscription
                }
            ); 
        }
    }

    dds_return_loan(dr, samples, (int32_t)MAX_SAMPLES);    
}


boost::mutex guard;

void send_discovery_event(const dds_entity_t dp, boost::asio::local::stream_protocol::socket *socket,
    const DiscoveryEvent& event)
{
    SPDLOG_DEBUG("Send discovery event");
    boost::async([socket, event]() {
        boost::mutex::scoped_lock scoped_lock(guard);
        auto payload = get_wire_format(event);
        scoped_lock.unlock();
        auto size = boost::asio::write(*socket, boost::asio::buffer(payload));
    });
}

void run_discovery(const dds_entity_t dp, boost::asio::local::stream_protocol::socket *socket)
{
    std::pair<bool, boost::asio::local::stream_protocol::socket *> pub_tx =
        std::make_pair(true, socket);
    std::pair<bool, boost::asio::local::stream_protocol::socket *> sub_tx =
        std::make_pair(false, socket);

    // publication events listener
    auto pub_listener = dds_create_listener(&pub_tx);
    dds_lset_data_available(pub_listener, on_data);
    auto pub_reader = dds_create_reader(dp, DDS_BUILTIN_TOPIC_DCPSPUBLICATION, NULL, pub_listener);
    
    // subscription events listener
    auto sub_listener = dds_create_listener(&sub_tx);
    dds_lset_data_available(sub_listener, on_data);
    auto sub_reader = dds_create_reader(dp, DDS_BUILTIN_TOPIC_DCPSSUBSCRIPTION, NULL, sub_listener);
}

//TODO: add qos
dds_entity_t create_forwading_dds_writer(const dds_entity_t &dp, const std::string &topic_name, 
                            const std::string &topic_type, const dds_qos_t &qos, const bool keyless)
{
    auto topic = cdds_create_blob_topic(
        dp, const_cast<char*>(topic_name.c_str()), const_cast<char*>(topic_type.c_str()), keyless
    );
    auto writer = dds_create_writer(dp, topic, &qos, NULL);
    if (writer < 0)
    {
        SPDLOG_ERROR(
            "Error creating DDS writer {0}",
            std::string(dds_strretcode(-writer))
        );
    }

    return writer;
}

void data_forwader_listener(const dds_entity_t dr, void *arg)
{
    auto arg_tuple = 
        reinterpret_cast<std::tuple<std::string, std::string, session_mgr::SessionMgr *> *>(arg);
    cdds_ddsi_payload *payload;
    dds_sample_info_t sample;
    SPDLOG_DEBUG("listener triggered");
    while (cdds_take_blob(dr, &payload, &sample) > 0)
    {
        if (sample.valid_data)
        {
                std::vector<uint8_t> bs(payload->payload, payload->payload + payload->size);
                SPDLOG_DEBUG("Data is valid!\n\tsize({0}, payload:({1})", 
                    bs.size(),
                    spdlog::fmt_lib::join(bs, " ")
                );
                /*
                SPDLOG_INFO("Route data from DDS\n topic: {0}\n payload: (key){1} (payload){2}",
                        std::get<0>(*arg_tuple),
                        std::get<1>(*arg_tuple),
                        bs);
                */
                (*payload).payload = NULL;
            //TODO: implement forwading payload throw SessionMgr
        } else {
            SPDLOG_WARN("Not valid data in payload");
        }

        cdds_serdata_unref(reinterpret_cast<ddsi_serdata*>(payload));
    }
}

//TODO: add qos
dds_entity_t create_forwading_dds_reader(const dds_entity_t &dp, const std::string &topic_name,
                            const std::string &topic_type, const std::string &key, const dds_qos_t &qos, const bool keyless, 
                            const std::chrono::system_clock::time_point &read_period, 
                            rodds::session_mgr::SessionMgr* session)
{
    auto topic = cdds_create_blob_topic(
        dp, const_cast<char*>(topic_name.c_str()), const_cast<char*>(topic_type.c_str()), keyless);

    //TODO: implement reader with duration read

    //if (read_period == std::chrono::system_clock::time_point())
    if (true)
    {
        std::tuple<std::string, std::string, session_mgr::SessionMgr *> arg =
            std::make_tuple(topic_name, key, session);
        auto listener = dds_create_listener(&arg);
        dds_lset_data_available(listener, data_forwader_listener);
        auto qos = dds_create_qos();
        auto reader = dds_create_reader(dp, topic, NULL, listener);

        if (reader >= 0)
        {
            SPDLOG_DEBUG("Reader initialized!");
            auto res = dds_reader_wait_for_historical_data(reader, DDS_100MS_DURATION);
            if (res < 0)
                SPDLOG_WARN("Error while calling dds_reader_historical_data retcode: {0}",
                            dds_strretcode(-res));
            else
                SPDLOG_DEBUG("Ok!");
            
            return reader;
        } else {
            SPDLOG_WARN("Error to create dds_reader retcode: {0}", 
                        dds_strretcode(-reader));
        }
    } else {
        //TODO: implement read data for some period @read_period
    }

    return topic;
}

void delete_dds_entity(dds_entity_t &dp)
{
    auto res = dds_delete(dp);   
    if (res > 0)
        SPDLOG_WARN("Error to delete DDS entity code: {0}", res);
}

std::string get_guid(const dds_entity_t &dp)
{
    dds_guid_t guid;
    auto res = dds_get_guid(dp, &guid);
    if (res != 0)
    {
        SPDLOG_ERROR("Error getting GUID of DDS entity code: {0}", res);
        return "";
    }

    return get_hex_string(std::begin(guid.v), std::end(guid.v)); 
}

} // namespace rodds::dds_discovery
