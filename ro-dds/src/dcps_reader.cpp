#include <exception>
#include <iostream>
#include <spdlog/common.h>
#include <thread>
#include <chrono>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/process.hpp>

#include <spdlog/spdlog.h>

#include "rodds/dds_discovery.hpp" 


int main()
{
    /*
    boost::asio::io_service s;
    boost::thread t([](){
        std::system("ros2 run demo_nodes_cpp talker");
        });
    t.detach();
    spdlog::set_level(spdlog::level::trace);
    SPDLOG_INFO("create dds participant");
    const dds_entity_t dp = dds_create_participant(0, NULL, NULL);
    boost::asio::io_service io_service;
    boost::asio::local::stream_protocol::socket tx(io_service), rx(io_service);
    boost::asio::local::connect_pair(tx, rx);

    SPDLOG_INFO("generate reader/writer"); 


    rodds::dds_discovery::DiscoveryEvent de;
    auto bufs = boost::asio::buffer(
            &de, sizeof(rodds::dds_discovery::DiscoveryEvent)
        );
    boost::asio::async_read(rx, bufs,
        [&de, &io_service, &t](const boost::system::error_code &error, std::size_t bytes_trans) 
        {
            assert(!error);
            assert(bytes_trans == sizeof(rodds::dds_discovery::DiscoveryEvent));
        });
    
    rodds::dds_discovery::run_discovery(dp, &tx);

    boost::asio::io_service::work work(io_service); 
   
    io_service.run();
    auto thread = boost::thread(
            boost::bind(&boost::asio::io_service::run, &io_service)
        );

    auto time_start = std::chrono::system_clock::now();

    while (std::chrono::duration_cast<std::chrono::seconds>(
        std::chrono::system_clock::now() - time_start).count() < 15) 
    {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    
    SPDLOG_INFO("done");
    io_service.stop();
    */
    return 0;
}
