#include "rodds/session_mgr.hpp"

#include <boost/chrono.hpp>
#include <boost/thread/thread.hpp> 

int main()
{
    rodds::session_mgr::SessionMgr sm(1);
    std::string key = "key";
    std::string event = "event";
   
    std::cout << "time start" << std::endl;    
    boost::this_thread::sleep_for(boost::chrono::seconds(5));
    std::cout << "time end" << std::endl;
 

    while(true)
    {
        sm.pub_discovery_event(key, event);
    }
    return 0;
}
