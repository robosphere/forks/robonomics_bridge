#pragma once
#include "rodds/dds_discovery.hpp"
#include "rodds/session_mgr.hpp"
#include <boost/asio/io_service.hpp>
#include <boost/asio/local/stream_protocol.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/smart_ptr/scoped_ptr.hpp>
#include <memory>

extern "C"
{

#include <cdds/cdds_builtin.h>
#include <cdds/cdds_util.h>

}

namespace rodds
{
struct RouteFromDds
{
    dds_entity_t reader;
    std::string publisher_key;
    std::vector<std::string> remote_readers;
    std::vector<std::string> local_writers;  

};

struct RouteToDds
{
    dds_entity_t writer;
    std::string subscriber_key;
    std::vector<std::string> remote_writers;
    std::vector<std::string> local_readers;
};

class RoDdsManager
{
public:
    RoDdsManager(boost::asio::io_service& ios, 
                 boost::asio::local::stream_protocol::socket& tx,
                 boost::asio::local::stream_protocol::socket& rx);
    // launch bridge
    void run();
    // run local dicovery process to identify new dds topics && create remote writers
    void run_local_discovery();
    // run remote discovery process to identify new dds topics routed from oposite side of bridge
    void run_routes_discovery(zmq::socket_t *dds_disco_rcv);

    // add/remove discovered dds writers/readers
    void add_dds_writer(const dds_discovery::DDSEntity &entity);
    void add_dds_reader(const dds_discovery::DDSEntity &entity);
    void remove_dds_writer(const std::string &entity_key);
    void remove_dds_reader(const std::string &entity_key);
    
    // add discovered routes dds entities
    void add_routes_from_dds(const std::string &key, const RouteFromDds &rfd);
    void add_routes_to_dds(const std::string &key, const RouteToDds &rtd);

    // try to add new route from dds to bridge (with pairing dds writers/readers and bridge pub/sub)
    dds_discovery::RouteStatus try_add_route_from_dds(const std::string &key, 
        const std::string &topic_name, const std::string &topic_type, 
        const bool keyless);

    // try to add new route discovered by bridge to dds (with pairing dds writers/readers and bridge pub/sub)
    dds_discovery::RouteStatus try_add_route_to_dds(const std::string &key,
        const std::string &topic_name, const std::string &topic_type,
        const bool keyless);
private:

    void read_local_event();
    
    void on_read_local_event_length(const boost::system::error_code &error,
                                    std::size_t bytes_transdered);
    
    void on_read_local_event(const boost::system::error_code &error,
                             std::size_t bytes_transferd);
private:
    //TODO parse startup configuration
    //Config config; 
    dds_entity_t _dp;
    session_mgr::SessionMgr _session;

    //Store of all local readers/writers
    std::map<std::string, dds_discovery::DDSEntity> _writers;
    std::map<std::string, dds_discovery::DDSEntity> _readers;

    //Store of all topic routed to/from bridge connection
    std::map<std::string, RouteFromDds> _routes_from_dds;
    std::map<std::string, RouteToDds> _routes_to_dds;

    //Local boost sockets data
    boost::asio::io_service& _loc_ios;
    boost::asio::local::stream_protocol::socket& _loc_rcv;
    boost::asio::local::stream_protocol::socket& _loc_snd;
    boost::asio::streambuf _loc_buf;
};

} //namespace rodds
