import time
import robonomicsinterface

def sub_handler(obj, update_nr, subscription_id):
    bytelist = obj['params']['result']['data']
    bytestr = ''.join(chr(val) for val in bytelist)
    print("catch: ", bytestr)

if __name__== '__main__':
    # prepare robonomics pub/sub
    account = robonomicsinterface.Account(remote_ws="ws://127.0.0.1:9944")
    pubsub = robonomicsinterface.PubSub(account)

    topic = b"/@rodds/loc_disco"

    # start catch discovery process
    print(pubsub.listen("/ip4/127.0.0.1/tcp/44440"))
    time.sleep(1)
    print(pubsub.connect("/ip4/127.0.0.1/tcp/44441"))
    time.sleep(1)
    print(pubsub.subscribe(str(topic), result_handler=sub_handler))
