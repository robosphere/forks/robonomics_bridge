import time
import robonomicsinterface
import threading
import zmq

if __name__ == "__main__":
    # prepare robonomics pub/sub
    account = robonomicsinterface.Account(remote_ws="ws://127.0.0.1:9991")
    pubsub = robonomicsinterface.PubSub(account)
    print(pubsub.listen("/ip4/127.0.0.1/tcp/44441"))
    time.sleep(1)
    print(pubsub.connect("/ip4/127.0.0.1/tcp/44440"))
    time.sleep(1)

    # prepare zmq pub/sub
    context = zmq.Context(1)
    subscriber = context.socket(zmq.SUB)
    address = "ipc:///tmp/writer.ipc"
    topic = b"/@rodds/loc_disco"
    print("connect to: ", address)
    subscriber.connect(address)
    print("subscribe to topic: ", topic)
    subscriber.setsockopt(zmq.SUBSCRIBE, topic)

    # start route discivery event process
    while True:
        # Read envelope with address
        [address, contents] = subscriber.recv_multipart()
        print(f"[{address}] {contents}")
        print("pub: ", pubsub.publish(str(topic), "data_" + str(contents)))

    # We never get here but clean up anyhow
    subscriber.close()
    context.term()
