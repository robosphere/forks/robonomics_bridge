# robonomics_bridge

## Motivation

The ROS2 framework is based on a protocol called DDS or Data Distraction Service, which ensures that ROS nodes interact with each other. This protocol is focused on working in a local network, it contains mechanisms for automatic detection of devices and very intensive interaction between them. However, in cases where it is necessary to ensure the operation of individual ROS nodes with some kind of external service or system, a security problem arises - after gaining access to one node, a third-party service gets access to all the others - it can completely listen to all internal DDS traffic.

To solve the problem of nodes accessing each other, you can use the [ROS2 Security package](https://github.com/ros2/sros2), which configures data access policies. However, this approach has a big disadvantage - all traffic in the system is encrypted, which can affect performance with the network load that DDS creates. Another solution is to create so-called gateways that publish the necessary data while keeping everything else private. As a rule, this is achieved using VPN or SSH tunnels between interacting nodes. This approach has been applied in projects such as Husarnet, Integration Service and Zenoh, but it is not without drawbacks. In this case, you need to create a secure communication channel that is tightly linked to the IP address or domain name of the counterparty, and they tend to change, block and be attacked.

The problem can be circumvented with the help of p2p technologies, on which the Robonomics blockchain is built. In fact, in this case, the blockchain can be a very secure routing table, in which the addresses of the counterparties are the public keys of the nodes. You need a local blockchain node and knowledge of which public key your counterparty has in order to no longer depend on the DNS system and IP address locks. The libp2p library, on which the Robonomics blockchain is built, can use different transport protocols to deliver messages. However, this is not all the possibilities that the blockchain can provide.

## Requirements
- Python (version >= 3.8.x)
- Boost (version >= 1.71.x)
- Cmake (version >= 3.12.x)
- [Robonomics node](https://github.com/airalab/robonomics) (version >= 2.0.x)
- [CycloneDDS](https://github.com/eclipse-cyclonedds/cyclonedds) (version 0.7.x)

## Installation
Clone repo
```bash
git clone git@gitlab.com:Splinter1984/robonomics_bridge.git
cd robonomics_bridge
git submodule init && git submodule sync && git submodule update
mkdir build && cd build
cmake ..
make .
```

## Description
This version of the project allows you to send events about the start/stop of dds topics.

## Usage
Launch 2 robonomics node
One for pub:
```bash
./robonomics --dev --tmp -l rpc=trace
```
Second for sub:
```bash
./robonomics --dev --tmp --ws-port 9991 -l rpc=trace
```
Launch robonomics publisher script
```bash
python ${PROJECT_DIR}/ro-dds/test/session_sub.py
```
Launch robonomics subscriber script
```bash
python ${PROJECT_DIR}/ro-dds/test/robonomics/ro_client.py
```
Start bridge session
```bash
./{PROJECT_DIR}/build/ro-dds/src/forwading_dds_reader
```
Launch any of ros2 node (for example talker)
```bash
ros2 run demo_nodes_cpp talker
```
See `ro_client.py` will publish information about running/stopped nodes to the terminal.

## Support
email: rom.andrianov1984@gmail.com

## Project status
The project is under active development.
